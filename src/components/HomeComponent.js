import React, { Component } from 'react';

class HomeComponent extends Component {
    constructor(props){
        super(props)
        this.state = {
            userName: '',
            password: ''
        }
    }
    loginUser = (e) => {
        e.preventDefault();
        let user = {userName: this.state.userName, 
                    password: this.state.password
                };
                console.log(user);
    }
    render() {
        return ( 
          <div className="container">
              <br/>
              <form>
                  <div className="form-group">
                      <label>User Name:</label>
                      <input type="text" className="form-control" id="userName" placeholder="Enter User Name" 
                          value={this.state.userName} onChange={this.changeUserNameHandler} name="userName"/>
                  </div>
                  <div className="form-group">
                      <label>Phone Number:</label>
                      <input type="password" className="form-control" id="password" placeholder="Enter Password" 
                          value={this.state.password} onChange={this.changePasswordHandler} name="password"/>
                  </div>
                  <button className="btn btn-success" onClick={this.addUser}>Login</button>
              </form>
              </div>
         )
      }
}
export default HomeComponent
