import React from 'react';
import UserService from '../services/UserService';

class UserComponent extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            users:[]
        }
        this.addUser = this.addUser.bind(this)
        this.deleteUser = this.deleteUser.bind(this)
        this.editUser = this.editUser.bind(this)
    }

    componentDidMount(){
        UserService.getUsers().then((response) => {
            console.log(response.data)
            this.setState({ users: response.data})
        });
    }

    editUser(id) {
        this.props.history.push(`/add-user/${id}`);
    }

    deleteUser(id){
        UserService.deleteUser(id).then(res=>{
            this.setState({users: this.state.users.filter(user => user.id!==id)})
        });
    }
    

    addUser(){
        this.props.history.push('/add-user/_add');
    }

    render (){
        return (
            <div className="container">
                <br/>
                <div className="row">
                    <input type="submit" onClick={this.addUser} value="Add User"/>
                </div>
                <h1 className = "text-center"> Users List</h1>
                <table className = "table table-bordered table-hover">
                    <thead>
                        <tr>
                            <td> First Name </td>
                            <td> Last Name </td>
                            <td> Phone Number </td>
                            <td> Action </td>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.users.map(
                                user => 
                                <tr key = {user.id}>   
                                     <td> {user.firstName}</td>   
                                     <td> {user.lastName}</td>   
                                     <td> {user.phoneNumber}</td>
                                     <td><button className="btn btn-success" onClick={()=>this.editUser(user.id)}>Edit</button>&nbsp;&nbsp;&nbsp;
                                     <button className="btn btn-success" onClick={()=>this.deleteUser(user.id)}>Delete</button></td>
                                </tr>
                            )
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}

export default UserComponent