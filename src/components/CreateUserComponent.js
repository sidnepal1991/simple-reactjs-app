import React, { Component } from 'react';
import UserService from '../services/UserService';

class CreateUserComponent extends Component {
    constructor(props){
        super(props)
        this.state = {
            id: this.props.match.params.id,
            firstName: '',
            lastName: '',
            phoneNumber: '',
            userName: '',
            password: ''
         }
         this.changeFirstNameHandler = this.changeFirstNameHandler.bind(this);
         this.changeLastNameHandler = this.changeLastNameHandler.bind(this);
         this.changePhoneNumberHandler = this.changePhoneNumberHandler.bind(this);
         this.changeUserNameHandler = this.changeUserNameHandler.bind(this);
         this.changePasswordHandler = this.changePasswordHandler.bind(this);
         this.addUser = this.addUser.bind(this);
    }
    componentDidMount(){
        if(this.state.id === '_add'){
            return
        }else{
            UserService.getUserById(this.state.id).then( (res) =>{
                let user = res.data;
                this.setState({firstName: user.firstName,
                    lastName: user.lastName,
                    phoneNumber : user.phoneNumber,
                    userName: user.userName,
                    password: user.password
                });
            });
        }        
    }

    handleValidation(){
        let formIsValid = true;
        if(this.state.firstName.length<=0 || this.state.lastName.length<=0 || 
            this.state.phoneNumber.length<=0 || 
            this.state.userName.length<=0 || this.state.password<=0){
            formIsValid = false;
        }
        return formIsValid;
    }

    addUser = (e) => {
        if(this.handleValidation()){
            e.preventDefault();
            let user = {firstName: this.state.firstName, lastName: this.state.lastName,
                phoneNumber: this.state.phoneNumber,
                userName: this.state.userName, password: this.state.password};
            console.log('user = ' + JSON.stringify(user));
            this.handleValidation(user);
            if(this.state.id === '_add'){
                UserService.createUser(user).then(res => {
                    console.log(res.data);
                    this.props.history.push('/userPage');
                });
            } else {
                UserService.editUser(user, this.state.id).then( res => {
                    this.props.history.push('/');
                });
            }
            alert("Form submitted");
         }else{
            alert("Form has errors.")
         }
    }

    cancel(){
        this.props.history.push('/');
    }

    changeFirstNameHandler= (event) =>{
        this.setState({firstName: event.target.value});
    }
    changeLastNameHandler= (event) =>{
        this.setState({lastName: event.target.value});
    }
    changePhoneNumberHandler= (event) =>{
        this.setState({phoneNumber: event.target.value});
    }
    changeUserNameHandler= (event) =>{
        this.setState({userName: event.target.value});
    }
    changePasswordHandler= (event) =>{
        this.setState({password: event.target.value});
    }
    getTitle(){
        if(this.state.id === '_add'){
            return <h3 className="text-center">Register</h3>
        } else{
            return <h3 className="text-center">Update Employee</h3>
        }
    }

    render() {
      return ( 
        <div className="container">
            <br/>
            <div className = "card col-md-6 offset-md-3 offset-md-3">
                {
                    this.getTitle()
                }
            <form>
                <div className="form-group">
                    <label>First Name:</label>
                    <input type="text" className="form-control" id="firstName" placeholder="Enter First Name" 
                        value={this.state.firstName} onChange={this.changeFirstNameHandler} name="firstName"/>
                </div>
                <div className="form-group">
                    <label>Last Name:</label>
                    <input type="text" className="form-control" id="lastName" placeholder="Enter Last Name" 
                        value={this.state.lastName} onChange={this.changeLastNameHandler} name="lastName"/>
                </div>
                <div className="form-group">
                    <label>Phone Number:</label>
                    <input type="text" className="form-control" id="phoneNumber" placeholder="Enter Phone Number" 
                        value={this.state.phoneNumber} onChange={this.changePhoneNumberHandler} name="phoneNumber"/>
                </div>
                <div className="form-group">
                    <label>User Name:</label>
                    <input type="text" className="form-control" id="userName" placeholder="Enter User Name" 
                        value={this.state.userName} onChange={this.changeUserNameHandler} name="userName"/>
                </div>
                <div className="form-group">
                    <label>Phone Number:</label>
                    <input type="password" className="form-control" id="password" placeholder="Enter Password" 
                        value={this.state.password} onChange={this.changePasswordHandler} name="password"/>
                </div>
                <button className="btn btn-success" onClick={this.addUser}>Submit</button>
                <button className="btn btn-danger" onClick={this.cancel.bind(this)} style={{marginLeft:"10px"}}>Cancel</button>
            </form>
            </div>
        </div>
       )
    }
}
  
  export default CreateUserComponent;
