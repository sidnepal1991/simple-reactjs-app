import React, { Component } from 'react';

class FooterComponent extends Component {
    constructor(props){
        super(props)
        this.state = {
        }
    }
    render() {
        return(
            <div>
                <footer>
                    <nav className="footer">
                        <span className="text-muted">All Right Reserved 2020 @SID</span>
                    </nav>
                </footer>
            </div>
        )
    }
}
export default FooterComponent
