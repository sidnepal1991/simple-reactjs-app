import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import HeaderComponent from './components/HeaderComponent';
import HomeComponent from './components/HomeComponent';
import CreateUserComponent from './components/CreateUserComponent';
import UserPageComponent from './components/UserPageComponent';

function App() {
  return (
    <div>
      <Router>
        <div>
          <HeaderComponent/>
            <div className="container">
              <switch>
                <Route path="/" component={HomeComponent} exact></Route> 
                <Route path="/userPage" component={UserPageComponent}></Route> 
                <Route path="/add-user/:id" component={CreateUserComponent}></Route> 
                </switch>
            </div>
        </div>
      </Router>
    </div>
  );
}

export default App;
