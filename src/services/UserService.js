import axios from 'axios'

const USERS_REST_API_URL = 'http://localhost:8081/api/v1';

class UserService {
    getUsers(){
        return axios.get(USERS_REST_API_URL+'/user');
    }
    createUser(user) {
        console.log(user);
        return axios.post(USERS_REST_API_URL+'/user', user);
    }
    deleteUser(id) {
        console.log(id);
        return axios.delete(USERS_REST_API_URL+'/user/'+id);
    }
    editUser(user, id) {
        return axios.put(USERS_REST_API_URL+'/userBy/'+id, user);
    }
    getUserById(id){
        return axios.get(USERS_REST_API_URL+'/user/'+id);
    }
}

export default new UserService();