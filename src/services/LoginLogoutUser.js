import axios from 'axios'

const USERS_REST_API_URL = 'http://localhost:8081/api/v1';

class LoginLogoutUser {
    loginUser(user) {
        console.log(user);
        return axios.post(USERS_REST_API_URL+'/user', user);
    }
}

export default new LoginLogoutUser();